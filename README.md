
## Original README.txt ##

```
Welcome to Seagull!

Seagull is a free and Open Source (GPL) multi-protocol 
traffic generator test tool. 
Primary aimed at IMS protocols (and thus being the perfect 
complement to SIPp (http://sipp.sourceforge.net) for IMS testing), 
Seagull is a powerful traffic generator for functional, load, 
endurance, stress and performance tests for almost any kind of protocol.

More information: http://gull.sourceforge.net/
```

## Install pre-requisites on Ubuntu. ##

```bash
sudo apt install build-essential curl git libglib2.0-dev ksh bison flex vim tmux
```

## Building ##

```bash
./get_external_deps.sh
ksh build-ext-lib.ksh
ksh build.ksh -target all
sudo cp bin/* /usr/local/bin

sudo mkdir -p /opt/albatross
sudo cp -r exe-env/* /opt/albatross
sudo chown -R $USER:$USER /opt/albatross

```